#Blue Life App

Repository to keep in track with the Blue Life Application development


this project its about a simple way to register volunteers for blue life.

to set it up you need django 1.6, with python 2.7 and mysqldb

you can do all this with a simple comand

```
#!bash

sudo apt-get install python  && sudo apt-get install django

```

```
#!bash

sudo apt-get install python-mysqldb


```

and you are all set up. also create a database called BlueLife in your local mysql-server

To deploy the application to the server, you need to install  gunicorn and nginx

you can install gunicorn with pip


```
#!bash

pip install gunicorn

```


```
#!bash

gunicorn ProjectName.wsgi

```
it will start runnning on localhost 8000 port


To install nginx run the folllowing
```
#!bash

sudo apt-get install nginx

```
#To get it working, 

go to your project directory and execute this command



by configuring nginx

inside the httpd brackets put this

```
#!nginx

server {
        listen 80 default;
        client_max_body_size 4G;
        server_name _;

        keepalive_timeout 5;

        # path for static files
        root /path/to/app/current/public;

        location / {
            # checks for static file, if not found proxy to app
            try_files $uri @proxy_to_app;
        }

        location @proxy_to_app {
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_redirect off;

            proxy_pass   http://app_server;
        }

        error_page 500 502 503 504 /500.html;
        location = /500.html {
            root /path/to/app/current/public;
        }
    }

```
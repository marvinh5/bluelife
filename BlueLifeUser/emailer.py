from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import datetime
import smtplib
import os


class Emailer(object):
    def __init__(self,mailfrom,mailto,username,password,subject,message):
        try:
            self.mailfrom =mailfrom;
            self.mailto = mailto;
            self.username = username;
            self.password = password;
            self.subject = subject;
            self.message = message;
        except:
            self.errorMessage = "error at Emailer __init__ message :'%s'"%Exception.message;
    def sendMessage(self):
        try:
            self.time = datetime.datetime.now();
            self.time = self.time - datetime.timedelta(days=1);
            self.time = self.time.strftime("%d-%m-%Y %H:%M");
            self.msg = MIMEMultipart();
            self.msg['Subject'] = self.subject
            self.msg['From'] = "Vida Azul";
            self.msg["To"]= ",".join(self.mailto);
            self.part1 = MIMEText(self.message,"plain");
            self.msg.attach(self.part1);
            s = smtplib.SMTP("smtp.gmail.com",587);
            s.starttls();
            s.login(self.username,self.password);
            s.sendmail(self.mailfrom,self.mailto,self.msg.as_string());
            s.close();
            print "mail sent \n";
        except Exception as e:
            print "mail not sent \n";
            self.errorMessage = "could'nt send message error message:'%s' "%e.strerror;
        
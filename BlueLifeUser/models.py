from django.db import models
from django import forms
import re
import time
import hashlib

# Create your models here.
sexChoises = (("M","Masculino"),("F","Femenino"))
activity_type = (("Report","Reporte"),("Cleaning","Limpieza"))
trashTypeslist = (("Cigarrillo","Cigarrillo"),("Envolturas de comida","Envolturas de comida"),
	("Botellas de bebida (plasticas)","Botellas de bebida (plasticas)"),
	("Bolsas Plasticas","Bolsas Plasticas"),
	("Botellas de aluminio","Botellas de aluminio"),
	("Tapas de botellas","Tapas de botellas"),
	("Bolsas de papel","bolsas de papel"),
	("Pajillas","Pajillas de bebida"))

class user(models.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=12)
	lastname = models.CharField(max_length=12)
	mail = models.CharField(max_length=60)
	ocupacion = models.CharField(max_length=12)
	age = models.IntegerField()
	sex = models.CharField(max_length=1,choices=sexChoises,default="M")

class trashTypes(models.Model):
	trashName = models.CharField(max_length=12)

class activity(models.Model):
	date_inserted    = models.DateTimeField(auto_now_add=True)
	date_last_update = models.DateTimeField(auto_now=True)
	user = models.ForeignKey(user,null=True)
	activityType = models.CharField(max_length=200, choices=activity_type,default="Report")
	trashWeigth = models.IntegerField()
	trashType = models.CharField(max_length=200)
	participants = models.IntegerField()
	geolocation = models.CharField(max_length=100)
	def trashTypeDict(self):
		value = self.trashType
		value = value.replace('{','')
		value = value.replace('}','')
		value = value.replace('"',"""""")
		list  = [x.split(':') for x in value.split(',')]
		return dict(list)
	def getPhotos(self):
		return activityPhotos.objects.all().filter(activity=self)
	def getlatitude(self):
		return self.getGeolacationDict()['latitude']
	def getLongitude(self):
		return self.getGeolacationDict()['longitude']
	def getName(self):
		return self.getGeolacationDict()['name']
	def getGeolacationDict(self):
		value = self.geolocation
		if(value[len(value)-1]==","):
			value = value[:len(value)-1]
		value = value.replace('{','')
		value = value.replace('}','')
		value = value.replace("'","""""")
		list  = [x.split(':') for x in value.split(',')]
		return dict(list)

def filename():
	timetoday  = time.time()
	return "{time}/".format(time=timetoday)


class activityPhotos(models.Model):
	activity = models.ForeignKey(activity)
	image = models.FileField(upload_to=filename())

	 
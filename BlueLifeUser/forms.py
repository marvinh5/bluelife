from django import forms
from BlueLifeUser.models import user, activity
from django.forms.util import ErrorList

class UserForm(forms.ModelForm):
	class Meta:
		model = user
		fields = ['name','lastname','mail','ocupacion',
		'age','sex']

class ActivityInformationForm(forms.ModelForm):
	class Meta:
		model =  activity
		fields = ['trashWeigth','trashType','participants','geolocation']
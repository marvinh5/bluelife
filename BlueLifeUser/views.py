from django.http import HttpResponse
from django.views.generic import View
from django.template.response import TemplateResponse
from BlueLifeUser import models,fileHandler
from BlueLifeUser.forms import UserForm,ActivityInformationForm
from django.shortcuts import redirect
from BlueLifeUser.models  import activityPhotos,user
from django.core.mail import send_mail
from email.mime.text import MIMEText
from emailer import Emailer




import smtplib

class ClientInformation(View):
	def get(self,request):
		sex_choises = dict(models.sexChoises)
		return TemplateResponse(request,'user.html',{'sexChoises':sex_choises})
	def post(self,request):
		pos = request.POST
		clientte = UserForm(pos,request.FILES)
		if clientte.is_valid()==False:
			return TemplateResponse(request,'user.html',{'sexChoises':dict(models.sexChoises),'form':clientte})
		if clientte.is_valid():
			client = clientte.save()
			request.session[0] = request.POST['mail'];
			request.session[1] = client.id;
			return redirect('/user/activityInformation',{'user':request.POST['mail']})

class ActivityInformation(View):
	def get(self,request):
		if request.session['0']:
			activities = dict(models.activity_type)
			form = ActivityInformationForm(request.POST,request.FILES);
			return TemplateResponse(request,'trashInfo.html',{'activities':activities,'trashTypes':dict(models.trashTypeslist)})
		else:
			return TemplateResponse(request,'forbidden.html',{})

	def post(self,request):
		form = ActivityInformationForm(request.POST);
		activities = dict(models.activity_type)
		if form.is_valid():
			activityModel = form.save()
			activityModel.user = user.objects.get(pk=request.session['1'])
			activityModel.save()
			for x in request.FILES.getlist("file"):
					activityPhotosVar = activityPhotos(activity=activityModel,image=x)
					activityPhotosVar.save()
			emailer = Emailer("Vida Azul",[request.session['0']],"vidazulvolunteers@gmail.com","password","Vida Azul Registro Activividades","Gracias por registrar su actividad nuestro equipo estara monitoreando.")
			emailer.sendMessage()
			return TemplateResponse(request,"congratz.html")
		else:
			return TemplateResponse(request,'trashInfo.html',{'form':form,'activities':activities,'trashTypes':dict(models.trashTypeslist)})
	def sendMail(self,mail):
		msg = MIMEText()
		msg["From"] = "BlueLife"
		msg["To"]   = mail
		s = smtplib.SMTP("localhost")
		s.sendMail("marvinnnz@gmail.com",[mail],msg.as_string())
		s.quit()





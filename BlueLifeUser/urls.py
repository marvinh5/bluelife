from django.conf.urls import url

from BlueLifeUser.views import ClientInformation
from BlueLifeUser.views import ActivityInformation

urlpatterns = [
	url(r'^$', ClientInformation.as_view()),
	url(r'^activityInformation', ActivityInformation.as_view())
]
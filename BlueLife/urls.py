from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
   	url(r'^$', include('BlueLifeUser.urls')),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^user/', include('BlueLifeUser.urls')),
    url(r'^manager/', include('BlueLifeManager.url'))
)

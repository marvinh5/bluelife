var map;
var latitude;
var longitude;

function loadMarkers(map){
$.getJSON("/static/beaches.json", function(data){ 
    $(data).each(function(idx, obj){ 
        $(obj).each(function(key, value){
            setMarker(map,value.latitude,value.longitude,value.name)
        });
    });
});
}
function setMarker(map,latitude,longitude, name)
{
  var myLatlng = new google.maps.LatLng(latitude,longitude);
  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: name
  });
  google.maps.event.addListener(marker, 'click', function() {

      document.querySelector("#geolocation").value ="{'latitude':'"+latitude+"','longitude':'"+longitude+"','name':'"+name+"'},";
      document.querySelector("#place").style.display="block";
      document.querySelector("#place").value=name;
  });
  
}


function initialize() {
  var mapOptions = {
    zoom: 12
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);

  loadMarkers(map);

  // Try HTML5 geolocation
  if(navigator.geolocation) {

    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
       position.coords.longitude);

      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;
      var infowindow = new google.maps.InfoWindow({
        map: map,
        position: pos,
      });

      map.setCenter(pos);
    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }

  $("#locationMessage").text("Localicacion");
  $("body").append("<style>"+
    "#map-canvas {"+
    "height: 200px;"+
    "margin: 0px;"+
    "width:"+$(".panel-body").width()+"px;"+
    "padding: 0px"+
    "position:absolute;"+
    "}"+
    "</style>");
  
}


function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position); 
}
 

google.maps.event.addDomListener(window, 'load', initialize
  );



$(document).ready(function(){
var currentPosition = 0;
$('.span2').slider().on('slide', function(ev){
    $("#TrashString").text("Peso Total basura Libras ("+ev.value+") :");
    var element = $("#trashweight");
    var currentWidth = element.width();
    var realposition = ev.value;
    if(currentPosition==realposition){return}
    if(currentPosition > realposition || currentPosition < realposition) 
    {
      if(((ev.value*100)/20)>=50)
    {
      $("#trashweight1")[0].style.display = "block";
      $("#trashweight2")[0].style.display = "block";
      $("#trashweight3")[0].style.display = "none";
    }
    if(((ev.value*100)/20)>=75)
    {
      $("#trashweight1")[0].style.display = "block";
      $("#trashweight2")[0].style.display = "block";
      $("#trashweight3")[0].style.display = "block";
    }
    if(((ev.value*100)/20)<50)
    {
      $("#trashweight1")[0].style.display = "block";
      $("#trashweight2")[0].style.display = "none";
      $("#trashweight3")[0].style.display = "none";
    }
    }else{
        
    }
    currentPosition = ev.value;
    //element.width(currentWidth+10);*/
    console.log(((ev.value*100)/20));
    
  })
$('.span2').slider('setValue',0)
$('.span3').slider('setValue',0)
 $('.span4').slider('setValue',0);
})

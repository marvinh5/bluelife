from django.shortcuts import render
from django.views.generic import View
from django.template.response import TemplateResponse,HttpResponse
from django.contrib.auth.forms import UserCreationForm
from BlueLifeUser.models import activity
from django.contrib.auth import authenticate,login
from django.shortcuts import redirect,render
import pdb
from django.contrib.auth import logout
from django.contrib.auth.models import User
# Create your views here.





class RegisterUser(View):
	def get(self,request):
		if request.user.is_authenticated():
			form  = UserCreationForm()
			return render(request, "newUser.html", {'form': form,})
	def post(self,request):
		if request.user.is_authenticated():
			try:
				user = User.objects.create_user(username=request.POST['username'], password=request.POST['password1'])
				pdb.set_trace()
				if user:
					return TemplateResponse(request,"congratz.html",{'name':request.POST['username'],'last_name':''})
				else:
					pdb.set_trace()
					return render(request, "newUser.html", {'form': form,})
			except Exception, e:
				form  = UserCreationForm()
				return render(request,"newUser.html", {'error': str([x for x in e]),'form':form})

class LogOut(View):
	def get(self,request):
		logout(request)
		return redirect("login/")

class Login(View):
	def get(self,request):
		return TemplateResponse(request,"login.html",{})

	def post(self,request):
		username = request.POST['username']
		password = request.POST['password']
		
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				login(request,user)
				return redirect('/manager/')
			else:
				return HttpResponse("Your account is not active");
		else:
			TemplateResponse3(request,"login.html",{'form.name.error':'usuario o contrasena incorrecta'})

class ActivityMainView(View):
	def get(self,request):
		if request.user.is_authenticated():
			activities = activity.objects.all()[:10]
			return TemplateResponse(request,"managerIndex.html",{'activities':activities})
		else:
			return redirect("login/")
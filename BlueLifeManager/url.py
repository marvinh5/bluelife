from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from BlueLifeManager.views import ActivityMainView,RegisterUser,Login,LogOut

urlpatterns = [
	url(r'^$', ActivityMainView.as_view()),
	url(r'^register', login_required(RegisterUser.as_view())),
	url(r'^login', Login.as_view()),
	url(r'^log_out', LogOut.as_view())
]